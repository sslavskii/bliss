import unittest
from bliss.space import power2

class TestCase(unittest.TestCase):
    def test_power2(self):
        expected = 4
        actual = power2(x=2)
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
